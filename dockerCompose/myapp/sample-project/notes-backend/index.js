const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 5000;

app.use(cors());
app.use(bodyParser.json());

let notes = [];

app.get('/api/notes', (req, res) => {
    res.json(notes);
});

app.post('/api/notes', (req, res) => {
    const newNote = req.body.note;
    if (newNote.trim() !== '') {
        notes.push(newNote);
        res.json({
            success: true
        });
    } else {
        res.json({
            success: false,
            error: 'Note cannot be empty'
        });
    }
});

app.delete('/api/notes/:index', (req, res) => {
    const index = parseInt(req.params.index);
    if (index >= 0 && index < notes.length) {
        notes.splice(index, 1);
        res.json({
            success: true
        });
    } else {
        res.json({
            success: false,
            error: 'Invalid index'
        });
    }
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});